const db = require('../infraestructure/database');
const uuid = require('uuid')
const moment = require('moment')

module.exports = {
    create: (u) => {

        let user = Object.assign(u, {
            ultimo_login: moment().format(),
            token: uuid()
        });

        return db.create('user', user);
    },
    findByEmail: (email) => {
        return db.find('user',{ email: email });
    },
    get: (id) => {
        return db.find('user',{ id: id });
    }
}