const userServices = require('../services/user.services');
const moment = require('moment');

module.exports = {
    create: async (req, h) => {
        let { nome, email, senha, telefones } = req.payload;

        if (!nome || !email || !senha || !telefones)
            return handleError(h, "Dados insuficientes", 400);

        try {

            let user = userServices.findByEmail(email)

            if (user != null)
                return handleError(h, "E-mail já existente", 400);

            let created = userServices.create(req.payload);

            if (created != null)
                return h.response(created).code(201);

            return handleError(h, "Nao foi possivel cadastrar o usuário. Contate o administrador", 500);

        } catch (error) {
            console.log(error);
            return handleError(h, "Ocorreu um erro inesperado", 500);
        }

    },
    signin: async (req, h) => {
        try {

            let { email, senha } = req.payload;

            if (!email || !senha)
                return handleError(h, "Dados insuficientes", 400);

            let user = userServices.findByEmail(email);

            if (user == null || user.senha != senha)
                return handleError(h, "Usuário e/ou senha inválidos", 401);

            return h.response(user).code(200);
        }
        catch (error) {
            return handleError(h, "Ocorreu um erro inesperado", 500);
        }
    },
    get: async (req, h) => {
        try {
            if (req.params.id == null)
                return handleError(h, "informe o id do usuario", 400);

            let authorization = req.headers.authorization;
            if (authorization == null)
                return handleError(h, "Não autorizado", 401);

            console.log(req.params.id);
            let user = userServices.get(req.params.id)
            if (user == null)
                return handleError(h, "usuario nao encontrado", 404);

            let authTokens = authorization.split(" ");

            if (authTokens[0].toLowerCase() != "bearer" || authTokens[1] == null)
                return handleError(h, "Não autorizado", 401);

            if (user.token != authTokens[1])
                return handleError(h, "Não autorizado", 401);

            let lastLogin = moment(user.ultimo_login);
            let agora = moment();

            if(moment().diff(moment(user.ultimo_login), 'minutes') > 30)
                return handleError(h, "sessao invalida", 401);

            return user;
        }
        catch(error){
            console.log(error);
            return handleError(h, "Ocorreu um erro inesperado", 500);
        }
    }
}

function handleError(h, msg, statusCode) {
    return h.response({ msg: msg }).code(statusCode);
}